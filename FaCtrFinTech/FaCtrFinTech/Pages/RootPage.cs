﻿
using Xamarin.Forms;

using FaCtrFinTech.Pages.Splash;
using FaCtrFinTech.ViewModels.Splash;
using FaCtrFinTech.Services;
using FaCtrFinTech.Statics;
using System.Threading.Tasks;
using FaCtrFinTech.ViewModels.Base;
using System.Collections.Generic;
using FaCtrFinTech.ViewModels;

namespace FaCtrFinTech.Pages
{


    public class RootPage : MasterDetailPage
    {
        Dictionary<MenuType, NavigationPage> Pages { get; set; }

        public RootPage()
        {
            Pages = new Dictionary<MenuType, NavigationPage>();
            Master = new MenuPage(this);
            BindingContext = new BaseViewModel(Navigation)
            {
                Title = "FaCtrFinTech",
                Icon = "slideout.png"
            };
            //setup home page
            NavigateAsync(MenuType.MainPage);
        }

        void SetDetailIfNull(Page page)
        {
            if (Detail == null && page != null)
                Detail = page;
        }

        public async Task NavigateAsync(MenuType id)
        {
            Page newPage;
            if (!Pages.ContainsKey(id))
            {
                switch (id)
                {
                    case MenuType.MainPage:
                        var page = new CRMNavigationPage(new MainMenuPage
                        { 
                                Title = "MainPage", // TextResources.MainTabs_Sales, 
                                Icon = new FileImageSource { File = "sales.png" }
                            });
                        SetDetailIfNull(page);
                        Pages.Add(id, page);
                        break;
                    /*
                     * case MenuType.About:
                        page = new CRMNavigationPage(new AboutItemListPage
                            { 
                                Title = TextResources.MainTabs_Products, 
                                Icon = new FileImageSource { File = "about.png" },
                                BindingContext = new AboutItemListViewModel() { Navigation = this.Navigation }
                            });
                        SetDetailIfNull(page);
                        Pages.Add(id, page);
                        break;
                        */
                }
            }

            newPage = Pages[id];
            if (newPage == null)
                return;

            //pop to root for Windows Phone
            if (Detail != null && Device.OS == TargetPlatform.WinPhone)
            {
                await Detail.Navigation.PopToRootAsync();
            }

            Detail = newPage;

            if (Device.Idiom != TargetIdiom.Tablet)
                IsPresented = false;
        }
    }

    public class RootTabPage : TabbedPage
    {
        public RootTabPage()
        {
            Children.Add(new CRMNavigationPage(new MainMenuPage
            { 
                        Title = "MainPage", // TextResources.MainTabs_Sales, 
                        Icon = new FileImageSource { File = "sales.png" }
                    })
                { 
                    Title = "MainPage", // TextResources.MainTabs_Sales, 
                    Icon = new FileImageSource { File = "sales.png" }
                });
            /*
            Children.Add(new CRMNavigationPage(new AboutItemListPage
                    { 
                        Title = TextResources.MainTabs_About, 
                        Icon = new FileImageSource { File = "about.png" },
                        BindingContext = new AboutItemListViewModel() { Navigation = this.Navigation }
                    })
                { 
                    Title = TextResources.MainTabs_About, 
                    Icon = new FileImageSource { File = "about.png" } 
                });
            */
        }

        protected override void OnCurrentPageChanged()
        {
            base.OnCurrentPageChanged();
            this.Title = this.CurrentPage.Title;
        }
    }

    public class CRMNavigationPage :NavigationPage
    {
        public CRMNavigationPage(Page root)
            : base(root)
        {
            Init();
        }

        public CRMNavigationPage()
        {
            Init();
        }

        void Init()
        {

            BarBackgroundColor = Palette._001;
            BarTextColor = Color.White;
        }
    }

    public enum MenuType
    {
        MainPage,
        About
    }

    public class HomeMenuItem
    {
        public HomeMenuItem()
        {
            MenuType = MenuType.About;
        }

        public string Icon { get; set; }

        public MenuType MenuType { get; set; }

        public string Title { get; set; }

        public string Details { get; set; }

        public int Id { get; set; }
    }
}

