﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;
using Plugin.Connectivity;

using FaCtrFinTech.Pages;
using FaCtrFinTech.Pages.Splash;
using FaCtrFinTech.Services;
using FaCtrFinTech.Statics;
using FaCtrFinTech.Localization;

namespace FaCtrFinTech
{
    public partial class App : Application
    {
        static Application app;
        public static Application CurrentApp
        {
            get { return app; }
        }

        readonly IAuthenticationService _AuthenticationService;

        public App()
        {
            app = this;
            // _AuthenticationService = DependencyService.Get<IAuthenticationService>();

            // If the App.IsAuthenticated property is false, modally present the SplashPage.
            /* if (!_AuthenticationService.IsAuthenticated) */
            if (true)
            {
                MainPage = new SplashPage();
            }
            else
            {
                GoToRoot();
            }

            InitializeComponent();
        }

        public static void GoToRoot()
        {
            /*
            if (Device.OS == TargetPlatform.iOS)
            {
                CurrentApp.MainPage = new RootTabPage();
            }
            else
            {
                
            }*/
            CurrentApp.MainPage = new MainPage();
        }

        public static async Task ExecuteIfConnected(Func<Task> actionToExecuteIfConnected)
        {
            if (IsConnected)
            {
                await actionToExecuteIfConnected();
            }
            else
            {
                await ShowNetworkConnectionAlert();
            }
        }

        static async Task ShowNetworkConnectionAlert()
        {
            await CurrentApp.MainPage.DisplayAlert(
                TextResources.NetworkConnection_Alert_Title,
                TextResources.NetworkConnection_Alert_Message,
                TextResources.NetworkConnection_Alert_Confirm);
        }

        public static bool IsConnected
        {
            get { return CrossConnectivity.Current.IsConnected; }
        }

        public static int AnimationSpeed = 250;

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
