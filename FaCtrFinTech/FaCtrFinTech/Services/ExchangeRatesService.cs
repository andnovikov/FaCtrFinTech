﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Reflection;
using Xamarin.Forms;
using FaCtrFinTech.Models.RatesModel;
using FaCtrFinTech.Services;
using Newtonsoft.Json;

[assembly: Dependency(typeof(ExchangeRatesService))]

namespace FaCtrFinTech.Services
{
    public class ExchangeRatesService : IExchangeRatesService
    {
        private static String RestUrl = "https://query.yahooapis.com/v1/public/yql?q=SELECT%20*%20FROM%20eurofx.daily&format=json&diagnostics=true&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&callback=";

        HttpClient client;

        public List<RateItem> Items { get; private set; }

        public ExchangeRatesService()
        {
            client = new HttpClient();
            client.MaxResponseContentBufferSize = 256000;
        }

        public async Task<List<RateItem>> GetLatestAsync()
        {
            
            Items = new List<RateItem>();

            var uri = new Uri(string.Format(RestUrl, string.Empty));

            try
            {
                var response = await client.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    string json = await response.Content.ReadAsStringAsync();
                    RatesData data = JsonConvert.DeserializeObject<RatesData>(json);

                    Items = data.query.results.cube;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }

            return Items;
        }
    }
}