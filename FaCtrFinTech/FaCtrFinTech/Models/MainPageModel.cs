﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaCtrFinTech.Models
{
    public class MainPageModel
    {
        public int Id { get; set; }
        public String Name { get; set; }
        public String Image { get; set; }
        public Guid guid { get; set; }
    }
}
