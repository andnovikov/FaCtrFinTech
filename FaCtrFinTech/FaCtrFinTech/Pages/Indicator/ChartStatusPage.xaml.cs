﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

using Syncfusion.SfChart.XForms;
using FaCtrFinTech.Statics;
using Xamarin.Forms;

using FaCtrFinTech.ViewModels.Chart;
using FaCtrFinTech.Models;
using FaCtrFinTech.Services;

namespace FaCtrFinTech.Pages.Indicator
{
    public class DataModel
    {

        public ObservableCollection<ChartDataPoint> HighTemperature { get; set; }
        public ObservableCollection<ChartDataPoint> LowTemperature { get; set; }

        public DataModel()
        {
            HighTemperature = new ObservableCollection<ChartDataPoint>();
            LowTemperature = new ObservableCollection<ChartDataPoint>();
        }

    }

    public partial class ChartStatusPage : ContentPage
    {
        readonly IWeatherForecastService _WeatherForecastService;

        

        public ChartStatusPage()
        {
            InitializeComponent();
            _WeatherForecastService = DependencyService.Get<IWeatherForecastService>();

            this.BackgroundColor = Palette._020;
            SfChart chart = (SfChart)WeatherChart;
            chart.BackgroundColor = Palette._020;

            //Initializing primary axis
            CategoryAxis primaryAxis = new CategoryAxis();
            primaryAxis.Title.Text = "Date";
            primaryAxis.Title.TextColor = Palette._021;
            chart.PrimaryAxis = primaryAxis;

            //Initializing secondary Axis
            NumericalAxis secondaryAxis = new NumericalAxis();
            secondaryAxis.Title.Text = "Documents";
            secondaryAxis.Title.TextColor = Palette._021;
            chart.SecondaryAxis = secondaryAxis;

            chart.PrimaryAxis.LabelStyle.TextColor = Palette._021;
            chart.SecondaryAxis.LabelStyle.TextColor = Palette._021;

            StackLayout chrtInfoData = (StackLayout)ChartInfoData;
            chrtInfoData.IsVisible = false;
            RelativeLayout chrtData = (RelativeLayout)ChartData;
            ChartData.IsVisible = false;

            LoadWeatherForeCast();
        }

        async void LoadWeatherForeCast()
        {
            SfChart chart = (SfChart)WeatherChart;


            try
            {
                //Initializing column series
                WeatherChannel channel = await _WeatherForecastService.GetForecastAsync();

                Label l = (Label)lHumidity;
                l.Text = channel.atmosphere.humidity.ToString();

                l = (Label)lPressure;
                l.Text = channel.atmosphere.pressure.ToString();

                l = (Label)lDaysCount;
                l.Text = channel.item.forecast.Count().ToString();

                DataModel dataModel = new DataModel();
                foreach (WeatherChartItem item in channel.item.forecast)
                {
                    dataModel.HighTemperature.Add(new ChartDataPoint(item.date, item.high));
                    dataModel.LowTemperature.Add(new ChartDataPoint(item.date, item.low));
                }

                chart.Series.Add(new StackingColumnSeries()
                {
                    ItemsSource = dataModel.HighTemperature,
                    GroupingLabel = "GroupOne",
                    Color = Palette._010
                });

                chart.Series.Add(new StackingColumnSeries()
                {
                    ItemsSource = dataModel.LowTemperature,
                    GroupingLabel = "GroupOne",
                    Color = Palette._026
                });
                /*
                StackingColumnSeries stackingColumnSeries2 = new StackingColumnSeries()
                {
                    ItemsSource = Data2,
                    GroupingLabel = "GroupTwo",
                    Label = "Bing",
                    XBindingPath = "Month",
                    YBindingPath = "Value"
                };
                */
            }
            catch (Exception ex)
            {
                /* 
                 * TODO: Sometimes its falling with error.
                 * Need to fix this bug.
                */
                Debug.WriteLine(@"				ERROR {0}", ex.Message);

                Label l = (Label)lHumidity;
                l.Text = "89";

                l = (Label)lPressure;
                l.Text = "1010";

                l = (Label)lDaysCount;
                l.Text = "10";

                
                DataModel dataModel = new DataModel();
                dataModel.HighTemperature.Add(new ChartDataPoint("20 Dec 2016", 52));
                dataModel.HighTemperature.Add(new ChartDataPoint("21 Dec 2016", 56));
                dataModel.HighTemperature.Add(new ChartDataPoint("22 Dec 2016", 59));
                dataModel.HighTemperature.Add(new ChartDataPoint("23 Dec 2016", 58));
                dataModel.HighTemperature.Add(new ChartDataPoint("24 Dec 2016", 53));
                dataModel.HighTemperature.Add(new ChartDataPoint("25 Dec 2016", 52));
                dataModel.HighTemperature.Add(new ChartDataPoint("26 Dec 2016", 51));
                dataModel.HighTemperature.Add(new ChartDataPoint("27 Dec 2016", 52));
                dataModel.HighTemperature.Add(new ChartDataPoint("28 Dec 2016", 52));
                dataModel.HighTemperature.Add(new ChartDataPoint("29 Dec 2016", 53));

                dataModel.LowTemperature.Add(new ChartDataPoint("20 Dec 2016", 35));
                dataModel.LowTemperature.Add(new ChartDataPoint("21 Dec 2016", 37));
                dataModel.LowTemperature.Add(new ChartDataPoint("22 Dec 2016", 43));
                dataModel.LowTemperature.Add(new ChartDataPoint("23 Dec 2016", 44));
                dataModel.LowTemperature.Add(new ChartDataPoint("24 Dec 2016", 46));
                dataModel.LowTemperature.Add(new ChartDataPoint("25 Dec 2016", 43));
                dataModel.LowTemperature.Add(new ChartDataPoint("26 Dec 2016", 42));
                dataModel.LowTemperature.Add(new ChartDataPoint("27 Dec 2016", 45));
                dataModel.LowTemperature.Add(new ChartDataPoint("28 Dec 2016", 45));
                dataModel.LowTemperature.Add(new ChartDataPoint("29 Dec 2016", 47));

                chart.Series.Add(new StackingColumnSeries()
                {
                    ItemsSource = dataModel.HighTemperature,
                    GroupingLabel = "GroupOne",
                    Color = Palette._010
                });

                chart.Series.Add(new StackingColumnSeries()
                {
                    ItemsSource = dataModel.LowTemperature,
                    GroupingLabel = "GroupOne",
                    Color = Palette._026
                });
            }

            StackLayout chrtInfoData = (StackLayout)ChartInfoData;
            chrtInfoData.IsVisible = true;
            RelativeLayout chrtData = (RelativeLayout)ChartData;
            ChartData.IsVisible = true;

            ActivityIndicator ac = (ActivityIndicator)LoadingIndicator;
            ac.IsRunning = false;
            ac.IsVisible = false;
        }

        void ClearLoadWeatherForeCast(object sender, EventArgs args)
        {
            SfChart chart = (SfChart)WeatherChart;
            chart.Series.Clear();
        }
    }
}
