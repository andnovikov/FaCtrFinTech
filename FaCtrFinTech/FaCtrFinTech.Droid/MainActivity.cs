﻿using System;

using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

using ImageCircle.Forms.Plugin.Droid;

namespace FaCtrFinTech.Droid
{
    [Activity(Label = "FaCtrFinTech", Icon = "@drawable/icon", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(bundle);

            global::Xamarin.Forms.Forms.Init(this, bundle);

            ImageCircle.Forms.Plugin.Droid.ImageCircleRenderer.Init();

            LoadApplication(new App());
        }
        
        protected override void OnDestroy()
        {
            base.Dispose();
            try
            {
                base.OnDestroy();
            }
            catch (Exception ex)
            {
                // Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }
            
        }
    }
}