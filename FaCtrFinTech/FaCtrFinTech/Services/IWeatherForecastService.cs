﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FaCtrFinTech.Models;


namespace FaCtrFinTech.Services
{
    interface IWeatherForecastService
    {
        Task<WeatherChannel> GetForecastAsync();
    }
}
