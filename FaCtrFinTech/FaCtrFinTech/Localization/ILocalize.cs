﻿

using System.Globalization;

namespace FaCtrFinTech.Localization
{
    public interface ILocalize
    {
        CultureInfo GetCurrentCultureInfo();
    }
}

