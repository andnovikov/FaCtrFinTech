﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;

using Xamarin.Forms;

using FaCtrFinTech.Models;
using FaCtrFinTech.Services;


[assembly: Dependency(typeof(WeatherForecastService))]

namespace FaCtrFinTech.Services
{
    public class WeatherForecastService : IWeatherForecastService
    {
        private static String RestUrl = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%3D2502265&format=json&callback=";

        HttpClient client;

        public List<WeatherChartItem> Items { get; private set; }

        public WeatherForecastService()
        {
            client = new HttpClient();
            client.MaxResponseContentBufferSize = 256000;
        }

        public async Task<WeatherChannel> GetForecastAsync()
        {
            WeatherChannel channel = new WeatherChannel();

            var uri = new Uri(string.Format(RestUrl, string.Empty));

            try
            {
                var response = await client.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    string json = await response.Content.ReadAsStringAsync();
                    WeathertData data = JsonConvert.DeserializeObject<WeathertData>(json);

                    channel = data.query.results.channel;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }

            return channel;
        }
    }
}
