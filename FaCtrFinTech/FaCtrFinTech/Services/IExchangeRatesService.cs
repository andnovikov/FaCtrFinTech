﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FaCtrFinTech.Models.RatesModel;

namespace FaCtrFinTech.Services
{
    interface IExchangeRatesService
    {
        Task<List<RateItem>> GetLatestAsync();
    }
}