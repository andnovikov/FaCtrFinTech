﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FaCtrFinTech.Models;

namespace FaCtrFinTech.ViewModels.MainPage
{
    public class MainPageViewModel
    {
        public List<MainPageModel> Items { get; set; }

        public MainPageViewModel()
        {
            Items = new List<MainPageModel>()
            {
                new MainPageModel { Name = "", Image = "indico01.png"},
                new MainPageModel { Name = "", Image = "indico02.png"},
                new MainPageModel { Name = "", Image = "indico03.png"},
                new MainPageModel { Name = "", Image = "indico04.png"},
                new MainPageModel { Name = "", Image = "indico05.png"}
            };
        }
    }
}