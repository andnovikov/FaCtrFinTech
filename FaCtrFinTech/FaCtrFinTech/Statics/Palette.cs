﻿
using Xamarin.Forms;

namespace FaCtrFinTech.Statics
{
    public static class Palette
    {
        /// <summary>
        ///  Primary color. Navigation bar on Android.
        /// </summary>
        public static readonly Color _001 = Color.FromHex("53BA9D");

        /// <summary>
        /// Password button color.
        /// </summary>
        public static readonly Color _002 = Color.FromHex("408D97");

        /// <summary>
        /// Ternary color. Accent color on Android.
        /// </summary>
        public static readonly Color _003 = Color.FromHex("8DE2D5");

        /// <summary>
        /// Android blue
        /// </summary>
        public static readonly Color _004 = Color.FromHex("00B9E6");

        /// <summary>
        /// iOS blue
        /// </summary>
        public static readonly Color _005 = Color.FromHex("0079FF");

        /// <summary>
        /// Primary (dark) label color.
        /// </summary>
        public static readonly Color _006 = Color.FromHex("4D4D4D"); 

        /// <summary>
        /// Secondary (light) label color.
        /// </summary>
        public static readonly Color _007 = Color.FromHex("999999");

        /// <summary>
        /// Another light label color. For the industry label in the customer details page.
        /// </summary>
        public static readonly Color _008 = Color.FromHex("D8D8D8");

        /// <summary>
        /// A dark medium gray with an extemely slight red hue. Used for chart backgrounds on Android.
        /// </summary>
        public static readonly Color _009 = Color.FromHex("5C5353");

        /// <summary>
        /// A medium gray. Used mostly for chart axis lines and fonts.
        /// </summary>
        public static readonly Color _010 = Color.FromHex("9DB8C9");

        /// <summary>
        /// A medium gray. Used for chart major grid lines and labels for iOS.
        /// </summary>
        public static readonly Color _011 = Color.FromHex("747474");

        /// <summary>
        /// A seperator color.
        /// </summary>
        public static readonly Color _012 = Color.FromHex("DDDDDD");

        /// <summary>
        /// A seperator color.
        /// </summary>
        public static readonly Color _013 = Color.FromHex("ededed");

        /// <summary>
        /// Orange 1.
        /// </summary>
        public static readonly Color _014 = Color.FromHex("EFA328");

        /// <summary>
        /// Orange 2.
        /// </summary>
        public static readonly Color _015 = Color.FromHex("DD852C");

        /// <summary>
        /// Orange 3.
        /// </summary>
        public static readonly Color _016 = Color.FromHex("C06F18");

        /// <summary>
        /// The pastel red color used for the grouping header for closed orders.
        /// </summary>
        public static readonly Color _017 = Color.FromHex("E28D8D");

        /// <summary>
        /// A gray. Used for title on splash screen.
        /// </summary>
        public static readonly Color _018 = Color.FromHex("C5C5C5");

        /// <summary>
        /// An off-white used on the About page.
        /// </summary>
        public static readonly Color _019 = Color.FromHex("F2F2F2");

        /// <summary>
        ///  Background color
        /// </summary>
        public static readonly Color _020 = Color.FromHex("35383D");

        /// <summary>
        /// Used for chart fonts.
        /// </summary>
        public static readonly Color _021 = Color.FromHex("FAFEFF");

        /// <summary>
        /// Used for chart fonts.
        /// </summary>
        public static readonly Color _022 = Color.FromHex("#1A1B1F");

        /// <summary>
        /// Used for green rounds.
        /// </summary>
        public static readonly Color _023 = Color.FromHex("#0CA97C");

        /// <summary>
        /// Used for yellow rounds.
        /// </summary>
        public static readonly Color _024 = Color.FromHex("#F8B003");
        
        /// <summary>
        /// Used for red rounds.
        /// </summary>
        public static readonly Color _025 = Color.FromHex("#F10D4A");

        /// <summary>
        /// Used for secondary chart axis line.
        /// </summary>
        public static readonly Color _026 = Color.FromHex("#2488AA");
    }
}

