﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using FaCtrFinTech.Pages.Indicator;
using FaCtrFinTech.ViewModels.MainPage;
using FaCtrFinTech.Models;
using FaCtrFinTech.Statics;

using Xamarin.Forms;
using ImageCircle.Forms.Plugin.Abstractions;

namespace FaCtrFinTech
{
    public partial class MainMenuPage : ContentPage
    {
        public MainPageViewModel model;

        public MainMenuPage()
        {
            // this.BindingContext = new MainPageViewModel();
            model = new MainPageViewModel();

            ScrollView scrollView = new ScrollView();
            this.Content = scrollView;

            var grid = new Grid()
            {
                Padding = 10
            };
            grid.BackgroundColor = Palette._020;
            scrollView.Content = grid;

            int rows = GetModulesCount() / 2 + GetModulesCount() % 2;
            for (int i = 1; i <= rows; i++)
            {
                grid.RowDefinitions.Add(new RowDefinition { Height = new GridLength(140) });
            }

            grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });
            grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) });

            int btnId = 0;
            for (int i = 0; i <= grid.RowDefinitions.Count-1; i++)
            {
                for (int j = 0; j <= grid.ColumnDefinitions.Count-1; j++)
                {
                    CircleImage cim = new CircleImage()
                    {
                        BorderColor = Palette._023,
                        Source = model.Items[btnId].Image,
                        BorderThickness = 1,
                        HorizontalOptions = LayoutOptions.Center,
                        VerticalOptions = LayoutOptions.Center

                    };
                    grid.Children.Add(cim, j, i);

                    if (btnId == 0)
                    {
                        cim.GestureRecognizers.Add(new TapGestureRecognizer()
                        {
                            NumberOfTapsRequired = 1,
                            Command = new Command(ChartButtonTapped)
                        });
                    }
                    else if (btnId == 1)
                    {
                        cim.GestureRecognizers.Add(new TapGestureRecognizer()
                        {
                            NumberOfTapsRequired = 1,
                            Command = new Command(EveningRepatStatusButtonTapped)
                        });
                    }
                    

                    /*
                    var btn = new Button {
                        Image = model.Items[btnId].Image,
                        BackgroundColor = Palette._020
                    };
                    model.Items[btnId].Id = btnId;
                    model.Items[btnId].guid = btn.Id;
                    btn.Clicked += OnButtonClicked;
                    grid.Children.Add(btn, j, i);
                    */
                    btnId++;
                    if (btnId == GetModulesCount())
                        break;
                }
                if (btnId == GetModulesCount())
                    break;
            }

            InitializeComponent();
        }

        void OnButtonClicked(object sender, EventArgs args)
        {
            int i = 0;
            Button btn = (Button)sender;
            if (btn != null)
            {
                foreach(MainPageModel item in model.Items)
                {
                    if (item.guid == btn.Id) { i = item.Id; }
                }
            }
            
            switch (i)
            {
                case 0: Navigation.PushAsync(new ChartStatusPage(), true);
                    break;
                case 1: Navigation.PushAsync(new EveningRepayStatusPage(), true);
                    break;
                default: break;
            }
        }

        void ChartButtonTapped()
        {
            Navigation.PushAsync(new ChartStatusPage(), true);
        }

        void EveningRepatStatusButtonTapped()
        {
            Navigation.PushAsync(new EveningRepayStatusPage(), true);
        }

        int GetModulesCount ()
        {
            return 5;
        }
    }
}
