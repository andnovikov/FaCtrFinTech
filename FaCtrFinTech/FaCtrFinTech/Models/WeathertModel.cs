﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaCtrFinTech.Models
{
    public class WeathertData
    {
        public WeatherQuery query;
    }

    public class WeatherQuery
    {
        public int count;
        public String created;
        public String lang;
        public WeatherResults results;

    }

    public class WeatherResults
    {
        public WeatherChannel channel;
    }

    public class WeatherAtmosphere
    {
        public int humidity;
        public float pressure;
        public int rising;
        public float visibility;
    }

    public class WeatherWind
    {
        public int chill;
        public int direction;
        public int speed;
    }

    public class WeatherChannel
    {
        public WeatherWind wind;
        public WeatherAtmosphere atmosphere;
        public WeatherItem item;
    }

    public class WeatherItem
    {
        public String title;
        public List<WeatherChartItem> forecast;
    }

    public class WeatherChartItem
    {
        public String date;
        public int high;
        public int low;
    }

    public class WeathertModel
    {
        public string Name { get; set; }
        public double Height { get; set; }
    }
}
