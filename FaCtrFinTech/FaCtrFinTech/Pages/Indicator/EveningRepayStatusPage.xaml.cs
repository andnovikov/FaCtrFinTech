﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FaCtrFinTech.Services;
using FaCtrFinTech.Models.RatesModel;
using Xamarin.Forms;

namespace FaCtrFinTech.Pages.Indicator
{
    public partial class EveningRepayStatusPage : ContentPage
    {
        readonly IExchangeRatesService _ExchangeRatesService;

        public EveningRepayStatusPage()
        {
            _ExchangeRatesService = DependencyService.Get<IExchangeRatesService>();
            InitializeComponent();
        }

        async void LoadExchangeRates(object sender, EventArgs args)
        {
            StackLayout sl = (StackLayout)RatesList;
            sl.Children.Clear();
            List<RateItem> items = await _ExchangeRatesService.GetLatestAsync();
            foreach(RateItem item in items) {
                var lb = new Label {};
                lb.Text = item.currency + ':' + item.rate;
                sl.Children.Add(lb);
            }
        }

        void ClearExchangeRates(object sender, EventArgs args)
        {
            StackLayout sl = (StackLayout)RatesList;
            sl.Children.Clear();
        }

        void CreateChartView(object sender, EventArgs args)
        {
            
        }
    }
}
