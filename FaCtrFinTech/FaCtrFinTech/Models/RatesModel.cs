﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace FaCtrFinTech.Models.RatesModel
{
    public class RatesData
    {
        public Query query;
    }

    public class Query
    {
        public int count;
        public String created;
        public String lang;
        public Results results;

    }

    public class Results
    {
        public List<RateItem> cube;
    }

    public class RateItem
    {
        public string currency;
        public string rate;
    }
}