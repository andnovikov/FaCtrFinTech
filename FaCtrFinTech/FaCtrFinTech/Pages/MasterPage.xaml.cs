﻿using System.Collections.Generic;
using Xamarin.Forms;
using FaCtrFinTech.Pages.Base;
using FaCtrFinTech.Statics;

namespace FaCtrFinTech
{

    public partial class MasterPage : ContentPage
	{
		public ListView ListView { get { return listView; } }

		public MasterPage ()
		{
			InitializeComponent ();

			var masterPageItems = new List<MasterPageItem> ();
            masterPageItems.Add(new MasterPageItem
            {
                Title = "Избранное",
                IconSource = "round_green.png",
                TargetType = typeof(MainMenuPage)
            });
            masterPageItems.Add (new MasterPageItem {
				Title = "Доступные лимиты по лицензии",
				IconSource = "round_green.png",
				TargetType = typeof(TestPage)
			});
            masterPageItems.Add(new MasterPageItem
            {
                Title = "Регламентные операции по кредитам",
                IconSource = "round_green.png",
                TargetType = typeof(TestPage)
            });
            masterPageItems.Add(new MasterPageItem
            {
                Title = "Выгрузка в бюро кредитных историй",
                IconSource = "round_green.png",
                TargetType = typeof(TestPage)
            });
            masterPageItems.Add(new MasterPageItem
            {
                Title = "Название группы индикаторов А",
                IconSource = "round_green.png",
                TargetType = typeof(TestPage)
            });

            listView.SeparatorVisibility = SeparatorVisibility.Default;
            listView.SeparatorColor = Palette._021;

            listView.ItemsSource = masterPageItems;
		}
	}
}
